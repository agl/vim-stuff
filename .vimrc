colorscheme molokai

set hidden
set packpath^=~/.vim
packadd minpac

let mapleader = ","

let g:acp_enableAtStartup = 0

let g:airline_enable_branch = 1
let g:airline_branch_empty_message = ''
let g:airline#extensions#tagbar#enabled = 0
let g:airline#extensions#ale#enabled = 1

let g:ale_change_sign_column_color = 1
let g:ale_sign_column_always = 1
let g:ale_cursor_detail = 1
let g:ale_list_vertical = 1
let g:ale_linters = {
      \ 'go': ['gofmt', 'golint', 'govet', 'gopls'],
      \}

let g:deoplete#enable_at_startup = 1

let g:html_indent_inctags = "html,body,head,tbody"
let g:html_indent_script1 = "inc"
let g:html_indent_style = "inc"

let g:go_fmt_command = "goimports"
let g:go_def_mode = "gopls"
let g:go_info_mode = "gopls"
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_interfaces = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1

let g:tagbar_autofocus = 1

let g:vaxe_prefer_openfl = 1
let g:vaxe_prefer_first_in_directory = 1

let g:vdebug_options = { 'port': 9009, 'ide_key': "IDESESSION"}

call minpac#init()
call minpac#add('k-takata/minpac', {'type': 'opt'})
call minpac#add('editorconfig/editorconfig-vim')
call minpac#add('vim-airline/vim-airline')
call minpac#add('fatih/vim-go', {'do': "silent! !cd $GOPATH && go get golang.org/x/tools/gopls@latest"})
call minpac#add('tpope/vim-eunuch')
call minpac#add('tpope/vim-fugitive')
call minpac#add('tpope/vim-markdown')
call minpac#add('vim-vdebug/vdebug')
call minpac#add('scrooloose/nerdtree')
call minpac#add('w0rp/ale')
call minpac#add('roxma/nvim-yarp', {'do': 'silent! !pip3 install pynvim'})
call minpac#add('roxma/vim-hug-neovim-rpc', {'do': 'silent! !pip3 install neovim'})
call minpac#add('roxma/LanguageServer-php-neovim', {'do': 'silent! !composer install && composer run-script parse-stubs'})
call minpac#add('Shougo/deoplete.nvim')
call minpac#add('xolox/vim-misc')
call minpac#add('xolox/vim-notes')
packloadall

command! PackUpdate packadd minpac | source $MYVIMRC | call minpac#update()
command! PackClean  packadd minpac | source $MYVIMRC | call minpac#clean()

inoremap kj <ESC>
map <Leader>s :Scratch<CR>
map <Leader>a :tabp<CR>
map <Leader>d :tabn<CR>
map <Leader>p :ptag<CR>
map <Leader>t :ter<CR>
map <Leader>ga :!git add %<CR>
map <Leader>t :term ++norestore ++close bash<CR>source $HOME/.bash_profile<CR>
map <Leader>nt :NERDTreeToggle<CR>
map <Leader>vv <C-w><C-t><C-w><C-H>
nnoremap <C-t> :tabnew<CR>
tnoremap <Esc> <C-\><C-n>
tnoremap <M-[> <Esc>
tnoremap <C-v><Esc> <Esc>

" golang shortcuts
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap <Leader>gp <Plug>(go-implements)
au FileType go nmap <Leader>gi <Plug>(go-info)
au FileType go nmap <Leader>gr <Plug>(go-rename)
au FileType go nmap <Leader>gb <Plug>(go-build)

" fix bash on windows arrow issues
let windows10=$WINDOWS10
if windows10 == '0'
  set t_ku=[A
  set t_kd=[B
  set t_kr=[C
  set t_kl=[D
endif

set nocompatible
set statusline+=%#warningmsg#
set statusline+=%*
set autoindent
set backspace=indent,eol,start
set expandtab
set laststatus=2
set rtp+=$GOROOT/misc/vim
set shiftwidth=2
set tabstop=2
set smarttab
syntax on
filetype plugin indent on
